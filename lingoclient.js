var socket = io();

socket.on('word', function(firstLetter) {
	reset(firstLetter);
    repaint();
});

socket.on('my result', function(guess, result) {
	console.log('my result: ' + result);
	for (var i = 0; i < 5; i++) {
		if (result[i] === 2) {
			myProgress[i] = guess[i];
		}
	}
    myGuesses.push(guess);
    myResults.push(result);
    drawMyBoard();
});

socket.on('opponent result', function(user, result) {
	console.log(user + ' -> ' + result);
	opponentResults.push(result);
    drawOpponentBoard();
});

var ctx = document.getElementById('canvas').getContext('2d');
ctx.font = '25px Monospace';

var height = 300, width = 250, side = 50;

var myGuess, myGuesses, myProgress, myResults, opponentResults;
reset();
repaint();

function reset(firstLetter) {
    if (!firstLetter) { firstLetter = ''; }
	myGuess = '';
    myGuesses = [];
	myProgress = [firstLetter, '', '', '', ''];
	myResults = [];
	opponentResults = [];
}

function repaint() { drawMyBoard(); drawOpponentBoard(); }

function drawMyBoard() {
    var x = 0.5, y = 0.5;
    drawBackground(x, y);
    drawInput(x, y, myGuess);
    var yStart = drawGuesses(x, y, myGuesses, myResults);
    drawHint(x, yStart, myProgress);
    drawGrid(x, y);
}

function drawOpponentBoard() {
    var x = 300.5, y = 0.5;
    drawBackground(x, y);
    drawResults(x, y, opponentResults);
    drawGrid(x, y);
}

function drawBackground(x, y) {
	ctx.fillStyle = 'white';
	ctx.fillRect(x, y, width, height);
}

function drawGrid(xOrigin, yOrigin) {
	ctx.beginPath();
	for (var x = 0; x <= width; x += side) {
		ctx.moveTo(xOrigin + x, yOrigin);
		ctx.lineTo(xOrigin + x, yOrigin + height);
	}
	for (var y = 0; y <= height; y += side) {
		ctx.moveTo(xOrigin, yOrigin + y);
		ctx.lineTo(xOrigin + width, yOrigin +  y);
	}
	ctx.strokeStyle = 'black';
	ctx.stroke();
}

function drawInput(xOrigin, yOrigin, input) {
	ctx.fillStyle = 'green';
	var x = xOrigin + side * (1/4);
	var y = yOrigin + side * (3/4);
	for (var i = 0; i < myGuess.length; i++) {
		ctx.fillText(myGuess[i], x, y);
		x += side;
	}
}

function drawGuesses(xOrigin, yOrigin, guesses, results) {
	var y = yOrigin + side * (7/4);
	var numGuesses = Math.min(4, guesses.length);
	for (var i = 0; i < numGuesses; i++) {
		var x = xOrigin + side * (1/4);
        var guess = guesses[guesses.length - numGuesses + i];
		var result = results[results.length - numGuesses + i];
		for (var j = 0; j < 5; j++) {
			if (result[j] === 1) {
				ctx.fillStyle = 'yellow';
				ctx.fillRect(x - side * (1/4), y - side * (3/4), side, side);
			} else if (result[j] === 2) {
				ctx.fillStyle = 'orange';
				ctx.fillRect(x - side * (1/4), y - side * (3/4), side, side);
			}
			ctx.fillStyle = 'green';
			ctx.fillText(guess[j], x, y);
			x += side;
		}
		y += side;
	}
	return y;
}

function drawResults(xOrigin, yOrigin, results) {
    var y = yOrigin + side * (7/4);
    var numResults = Math.min(4, results.length);
    for (var i = 0; i < numResults; i++) {
        var x = xOrigin + side * (1/4);
        var result = results[results.length - numResults + i];
        for (var j = 0; j < 5; j++) {
            if (result[j] === 1) {
                ctx.fillStyle = 'yellow';
                ctx.fillRect(x - side * (1/4), y - side * (3/4), side, side);
            } else if (result[j] === 2) {
                ctx.fillStyle = 'orange';
                ctx.fillRect(x - side * (1/4), y - side * (3/4), side, side);
            }
            x += side;
        }
        y += side;
    }
    return y;
}

function drawHint(xOrigin, yOrigin, progress) {
	var x = xOrigin + side * (1/4);
	for (var i = 0; i < 5; i++) {
		ctx.fillText(progress[i], x, yOrigin);
		x += side;
	}
}

function isCharacter(charCode) {
	return (charCode >= 65 && charCode <= 90)
	    || (charCode >= 97 && charCode <= 122);
}

// todo: draw only what is needed
document.addEventListener('keypress', function(e) {
	var keyCode = e.keyCode || e.charCode;
   // backspace
	if (keyCode === 8) {
      myGuess = myGuess.substr(0, myGuess.length - 1);
      repaint();
   }
	// character
	else if (isCharacter(keyCode)) {
      if (myGuess.length < 5) {
         myGuess += e.key;
         repaint();
      }
   }
	// return
	else if (keyCode === 13) {
      if (myGuess.length === 5) {
         socket.emit('guess', myGuess);
         myGuess = '';
         repaint();
      }
   }
});
