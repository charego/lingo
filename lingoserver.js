var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var fs = require('fs');

var acceptableGuesses = readSync('res/acceptableguesses.txt');
var possibleWords = readSync('res/possiblewords.txt');
var wordIndex = 0;
var word = '';
var players = [];

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/index.html');
});

app.get('/lingoclient.js', function(req, res) {
	res.sendFile(__dirname + '/lingoclient.js');
});

io.on('connection', function(socket) {
	var uuid = Math.random().toString(36).substring(2, 7);
	console.log('user connected: ' + uuid);
	players.push(uuid);
	if (players.length === 2) {
		var firstLetter = newGame();
		io.emit('word', firstLetter);
	}
	socket.on('disconnect', function() {
		console.log('user disconnected: ' + uuid);
		var playerIndex = players.indexOf(uuid);
		players.splice(playerIndex, 1);
	});
	socket.on('guess', function(guess) {
		var result = evaluate(guess);
		console.log(uuid + ': ' + guess + ' -> ' + result);
		socket.emit('my result', guess, result);
		socket.broadcast.emit('opponent result', uuid, result);
        if (isCorrect(result)) {
			var firstLetter = newWord();
			io.emit('word', firstLetter);
		}
	});
});

function isCorrect(result) {
    for (var i = 0; i < 5; i++) {
        if (result[i] !== 2) return false;
    }
    return true;
}

server.listen(3000, function() {
	console.log('listening on *:3000');
});

function evaluate(guess) {
	if (acceptableGuesses.indexOf(guess) === -1) {
		// todo: use static object
		return [0, 0, 0, 0, 0];
	}

	// the guess is acceptable
	var result = [5];
	var remaining = [];
	for (var i = 0; i < 5; i++) { 
		if (guess[i] === word[i]) { 
			result[i] = 2;
		} else {
			result[i] = 0;
			remaining[i] = word[i];
		}
	}
	for (var i = 0; i < 5; i++) {
		if (result[i] === 0) {
			var index = remaining.indexOf(guess[i]);
			if (index !== -1) {
				result[i] = 1;
				remaining[index] = null;
			}
		}
	}
	return result;
}

function newGame() {
	shuffle(possibleWords);
	wordIndex = 0;
	return newWord();
}

function newWord() {
	word = possibleWords[wordIndex++];
	console.log('new word: ' + word);
	return word[0];
}

function readSync(filename) {
	var params = { encoding : 'utf-8' };
	var string = fs.readFileSync(filename, params);
	return string.split("\n");
}

// Fisher-Yates shuffle
function shuffle(array) {
	var counter = array.length, temp, index;
	while (counter > 0) {
		index = Math.floor(Math.random() * counter);
		counter--;
		temp = array[counter];
		array[counter] = array[index];
		array[index] = temp;
	}
	return array;
}
